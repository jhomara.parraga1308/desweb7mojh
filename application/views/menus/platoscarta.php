<div class="row">
  <div class="col-md-12 text-center">
    <h1>~ MENÚ PLATOS A LA CARTA ~</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/pc1.png" alt="PC1" width="100%"></tr>
          <tr>Medallones de res en salsa de café</tr>
          <br>
          <tr>$3.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/pc2.jpg" alt="PC2" width="100%"></tr>
          <tr>Pato a la naranja con rollo primavera de verduras de temporada</tr>
          <br>
          <tr >$6.50 </tr>
        </td>
      </table>
    </div>

  </div>

  <div class="container" style="background-color: white;">
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/pc3.jpg" alt="PC3" width="100%"></tr>
          <tr>Cordon Blue con ensalada de espárragos</tr>
          <br>
          <tr>$5.50 </tr>
        </td>
      </table>
    </div>

  </div>
</div>
