<div class="row">
  <div class="col-md-12 text-center">
    <h1>~ MENÚ MERIENDAS ~</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/mr1.jpg" alt="Merienda1" width="100%"></tr>
          <tr>Rollitos rellenos de pollo y verduras, aderezos de salsa de soya y ajonjolí</tr>
          <br>
          <tr>$3.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/mr2.jpg" alt="Merienda2" width="100%"></tr>
          <tr>Judías verdes con patatas y pollo a la plancha</tr>
          <br>
          <tr >$4.50 </tr>
        </td>
      </table>
    </div>

  </div>

  <div class="container" style="background-color: white;">
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/mr3.jpg" alt="Merienda3" width="100%"></tr>
          <tr>Brochetas de pavo y verduras a la parrilla en salsa de vino blanco</tr>
          <br>
          <tr>$4.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-6 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/mer4.jpg" alt="Merienda4" width="100%"></tr>
          <tr>Atún con papas a la plancha con salteado de espárragos en ajonjolí</tr>
          <br>
          <tr >$4.50 </tr>
        </td>
      </table>
    </div>

  </div>
</div>
