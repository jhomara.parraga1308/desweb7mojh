<div class="row">
  <div class="col-md-12 text-center">
    <h1>~ MENÚ DESAYUNOS ~</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table>
        
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des1.jpg" alt="Desayuno1" width="100%"></tr>
          <tr>Desayuno Continental</tr>
          <br>
          <tr>$3.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des2.jpg" alt="Desayuno2" width="100%"></tr>
          <tr>Desayuno Americano</tr>
          <br>
          <tr >$3.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des3.jpg" alt="Desayuno3" width="100%"></tr>
          <tr>Desayuno oriental</tr>
          <br>
          <tr>$4.50 </tr>
        </td>
      </table>
    </div>
  </div>

  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des4.png" alt="Desayuno4" width="100%"></tr>
          <tr>Desayuno Manaba</tr>
          <br>
          <tr>$4.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des5.jpg" alt="Desayuno5" width="100%"></tr>
          <tr>Desayuno Vegano</tr>
          <br>
          <tr >$2.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/des6.jpg" alt="Desayuno6" width="100%"></tr>
          <tr>Desayuno a la Española</tr>
          <br>
          <tr>$4.50 </tr>
        </td>
      </table>
    </div>
  </div>
</div>
