<div class="row">
  <div class="col-md-12 text-center">
    <h1>~ MENÚ ALMUERZOS ~</h1>
  </div>
  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm1.jpg" alt="Almuerzo1" width="100%"></tr>
          <tr>Paella de camarones, y mejillones</tr>
          <br>
          <tr>$6.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm2.jpg" alt="Almuerzo2" width="100%"></tr>
          <tr>Estofado de carne, porción de arroz y ensalada</tr>
          <br>
          <tr >$3.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm3.jpg" alt="Almuerzo3" width="100%"></tr>
          <tr>Seco de pollo y ensalada</tr>
          <br>
          <tr>$3.50 </tr>
        </td>
      </table>
    </div>
  </div>

  <div class="container" style="background-color: white;">
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm4.jpg" alt="Merienda4" width="100%"></tr>
          <tr>Encebollado Manaba, porción de chifles + canguil</tr>
          <br>
          <tr>$2.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm5.jpg" alt="Merienda5" width="100%"></tr>
          <tr>Calamares rellenos con bacon y champiñones para 2 personas</tr>
          <br>
          <tr >$7.50 </tr>
        </td>
      </table>
    </div>
    <div class="col-md-4 text-center">
      <table>
        <td>
          <tr><img src="<?php echo base_url();?>assets/images/alm6.jpg" alt="Merienda6" width="100%"></tr>
          <tr>Carne a la BBQ, puré de papas y una ñporción de arroz</tr>
          <br>
          <tr>$4.50 </tr>
        </td>
      </table>
    </div>
  </div>
</div>
