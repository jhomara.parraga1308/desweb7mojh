<section class="carrusel">
        <div id="carouselExample" class="carousel slide">
            <div class="carousel-inner">
            <div class="carousel-item active">
                <img src="https://picsum.photos/1920/300/?blur" class="d-block w-100" alt="ttt">
            </div>
            </div>
        </div>
    </section>
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center">
				<h1><b>~ RESTAURANTE UTC ~</b></h1>
			</div>
			<div class="col-md-12 text-center">
				Restaurante en Latacunga <br>
Abierto hoy hasta las 22:00
			</div>
		</div>

		<div class="row">
			<div class="col-md-6 text-center">
				<h2>MISIÓN</h2>
			</div>
			<div class="col-md-6 text-center">
				<h2>VISIÓN</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 text-center">
				<p>“Nos dedicamos a satisfacer las necesidades gastronómicas de nuestros clientes, preparando y ofreciendo alimentos y servicios de la calidad más alta y una atención personalizada que asegure su satisfacción”.</p>
			</div>
			<div class="col-md-6 text-center">
				<p>Ser reconocidos por brindar a nuestros clientes sensaciones agradables y momentos felices.Posicionarnos en el corazón de las familias palmiranas y de todos los que nos visitan. Contribuir y aportar nuestro granito de arena, para generar una colombia feliz y en paz; que brinde un mejor futuro a nuestras próximas generaciones..</p>
			</div>
		</div>

		<div class="row">
			<div class="col-md-4 text-center">
				<h2>VISÍTANOS</h2>
			</div>
			<div class="col-md-8 text-center">
				<h2>UBICACIÓN</h2>
			</div>
		</div>
		<!-- maps -->
		<div class="row">
			<div class="col-md-4 text-center">
				<img src="<?php echo base_url(); ?>/assets/images/portada.jpg" alt="portadita" height="300px">
			</div>
			<div class="col-md-8 text-center" >
					<div id="mapaDireccion" style="width:100%; height: 600px;"></div>
			</div>
			</div>

<!-- segundo mapa -->
      <div class="row">
        <div class="col-md-12 text-center">
          <h2>Ubicación 2</h2>
        </div>
      </div>
      <div class="row">
        <div class="col-md-4 text-center">
          <img src="<?php echo base_url(); ?>/assets/images/portada.jpg" alt="sucursal" height="300px">
        </div>
        <div class="col-md-8 text-center">
          <div id="mapaDireccion2" style="width:100%; height: 600px;"></div>
        </div>
      </div>
    </div>


	</div>


	</div>
